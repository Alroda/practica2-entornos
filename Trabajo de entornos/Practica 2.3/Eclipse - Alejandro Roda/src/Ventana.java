import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTabbedPane;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JSlider;
import javax.swing.JSpinner;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import java.awt.Color;
import javax.swing.JEditorPane;
import javax.swing.JScrollPane;
import javax.swing.JToolBar;
import javax.swing.JDesktopPane;
import java.awt.Toolkit;
import javax.swing.ImageIcon;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Ventana extends JFrame {

	private JPanel contentPane;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private final ButtonGroup buttonGroup_1 = new ButtonGroup();

	/**
	 * Launch the application.
	 */
	/**
	 * 
	 * @author Alejandro Roda
	 * @since 15/05/2018
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ventana frame = new Ventana();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Ventana() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(Ventana.class.getResource("/com/sun/java/swing/plaf/windows/icons/Computer.gif")));
		setTitle("Alejandro Roda - Ventana");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 659, 474);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnArchivo = new JMenu("Archivo");
		menuBar.add(mnArchivo);
		
		JMenuItem mntmNewMenuItem = new JMenuItem("Guardar como");
		mnArchivo.add(mntmNewMenuItem);
		
		JMenuItem mntmGuardar = new JMenuItem("Guardar");
		mnArchivo.add(mntmGuardar);
		
		JMenu mnEditar = new JMenu("Editar");
		menuBar.add(mnEditar);
		
		JMenuItem mntmNewMenuItem_1 = new JMenuItem("Cortar");
		mnEditar.add(mntmNewMenuItem_1);
		
		JMenuItem mntmNewMenuItem_2 = new JMenuItem("Copiar");
		mnEditar.add(mntmNewMenuItem_2);
		
		JMenuItem mntmPegar = new JMenuItem("Pegar");
		mnEditar.add(mntmPegar);
		
		JMenu mnVentana = new JMenu("Ventana");
		menuBar.add(mnVentana);
		
		JMenu mnAyuda = new JMenu("Ayuda");
		menuBar.add(mnAyuda);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(0, 68, 643, 367);
		contentPane.add(tabbedPane);
		
		JPanel panel = new JPanel();
		tabbedPane.addTab("Menu principal", null, panel, null);
		panel.setLayout(null);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setForeground(Color.RED);
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"1", "2", "3", "4", "5"}));
		comboBox.setBounds(74, 95, 28, 20);
		panel.add(comboBox);
		
		JSlider slider = new JSlider();
		slider.setBackground(Color.PINK);
		slider.setSnapToTicks(true);
		slider.setPaintTicks(true);
		slider.setPaintLabels(true);
		slider.setValue(5);
		slider.setMaximum(10);
		slider.setMajorTickSpacing(1);
		slider.setBounds(10, 25, 200, 45);
		panel.add(slider);
		
		JSpinner spinner = new JSpinner();
		spinner.setBounds(10, 95, 29, 20);
		panel.add(spinner);
		
		JRadioButton rdbtnHombre = new JRadioButton("Hombre");
		rdbtnHombre.setSelected(true);
		buttonGroup.add(rdbtnHombre);
		rdbtnHombre.setBounds(6, 150, 109, 23);
		panel.add(rdbtnHombre);
		
		JRadioButton rdbtnMujer = new JRadioButton("Mujer");
		rdbtnMujer.setSelected(true);
		buttonGroup.add(rdbtnMujer);
		rdbtnMujer.setBounds(150, 150, 109, 23);
		panel.add(rdbtnMujer);
		
		JButton btnNewButton = new JButton("Aceptar");
		btnNewButton.setBackground(Color.GREEN);
		btnNewButton.setForeground(Color.CYAN);
		btnNewButton.setBounds(10, 203, 89, 23);
		panel.add(btnNewButton);
		
		JButton btnDenegar = new JButton("Denegar");
		btnDenegar.setBackground(Color.YELLOW);
		btnDenegar.setForeground(Color.MAGENTA);
		btnDenegar.setBounds(150, 203, 89, 23);
		panel.add(btnDenegar);
		
		JCheckBox chckbxCasado = new JCheckBox("Casado");
		chckbxCasado.setSelected(true);
		buttonGroup_1.add(chckbxCasado);
		chckbxCasado.setBounds(10, 265, 97, 23);
		panel.add(chckbxCasado);
		
		JCheckBox chckbxSoltero = new JCheckBox("Soltero");
		chckbxSoltero.setSelected(true);
		buttonGroup_1.add(chckbxSoltero);
		chckbxSoltero.setBounds(150, 265, 97, 23);
		panel.add(chckbxSoltero);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(351, 42, 153, 118);
		panel.add(scrollPane);
		
		JEditorPane editorPane = new JEditorPane();
		scrollPane.setViewportView(editorPane);
		
		JButton btnNewButton_1 = new JButton("New button");
		btnNewButton_1.setIcon(new ImageIcon(Ventana.class.getResource("/com/sun/java/swing/plaf/windows/icons/FloppyDrive.gif")));
		btnNewButton_1.setBounds(282, 203, 89, 23);
		panel.add(btnNewButton_1);
		
		JPanel panel_1 = new JPanel();
		tabbedPane.addTab("Contacto", null, panel_1, null);
		
		JToolBar toolBar = new JToolBar();
		toolBar.setBounds(10, 11, 227, 16);
		contentPane.add(toolBar);
		
		JButton btnNewButton_3 = new JButton("Abrir");
		btnNewButton_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		toolBar.add(btnNewButton_3);
		
		JButton btnNewButton_2 = new JButton("Guardar");
		toolBar.add(btnNewButton_2);
		
		JButton btnNewButton_4 = new JButton("Cerrar");
		toolBar.add(btnNewButton_4);
	}
}
