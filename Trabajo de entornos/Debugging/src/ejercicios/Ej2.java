package ejercicios;

import java.util.Scanner;

public class Ej2 {
	public static void main(String[] args){
		/*Establecer un breakpoint en la primera instrucción y avanzar
		 * instrucción a instrucción (step into) analizando el contenido de las variables
		 */
		
		Scanner input ;
		int numeroLeido;
		String cadenaLeida;		
		
		input = new Scanner(System.in);
		
		//El error esta en que leemos por teclado primero el numero entero que el string
		//Para solucionarlo pedimos primero el string
		
		System.out.println("Introduce un numero ");
		numeroLeido = input.nextInt();
		
		System.out.println("Introduce un numero como String");
		cadenaLeida = input.nextLine();
		
		if ( numeroLeido == Integer.parseInt(cadenaLeida) )
			System.out.println("Lo datos introducidos reprensentan el mismo número");
		
		
		input.close();
		
		
		
	}
}
